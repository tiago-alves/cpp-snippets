//	Tiago Alves 17/10/2021
//	updated in 17/09/2022

/*
	Pythagorean Theorem on a function

	a^2 + b^2 = x
	<=> c = sqrt(x)

	https://en.wikipedia.org/wiki/Pythagorean_theorem

	Also, good read
	https://stackoverflow.com/questions/2940367/what-is-more-efficient-using-pow-to-square-or-just-multiply-it-with-itself
*/

#include <iostream>
#include <cmath>

//	THIS

#define PYTHAGORAS(A,B) 	sqrt((A*A)+(B*B))

//	OR THIS

float Pythagoras(float _fa, float _fb)
{
	return sqrt((_fa * _fa) + (_fb * _fb));	// returns (Sqroot of sum)
}

int main()
{
	//	Pratical example

	printf("%f\n", Pythagoras(1, 2));
	printf("%f\n", PYTHAGORAS(1, 2));

	return 0;
}